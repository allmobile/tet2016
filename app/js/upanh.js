'use strict';
angular.module('tet2016')
    .controller('UpanhCtrl', ['createDialog', '$rootScope', '$scope', 'RESOURCES', 'REQUEST', 'UserService', '$controller', 'Upload', function(createDialog, $rootScope, $scope, RESOURCES, REQUEST, UserService, $controller, Upload) {
        $rootScope.sound_enable = true;

        REQUEST.postData(RESOURCES.API_SERVICE_SERVICE + 'get-random-caption', {}, function(response) {
            var data = response.data;
            //console.log(data.caption);
            if (data.caption) {
                $scope.caption = data.caption;
            }
            setTimeout(function() {

                $scope.$apply();

            }, 0);
        });


        if (!UserService.islogged()) {
            $scope.loginDialog();
        }

        var dataURItoBlob = function(dataURI, file) {
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            var blob = new Blob([new Uint8Array(array)], {
                type: mimeString
            });

            return new File([blob], file.name);
        };



        $scope.uploading = false;
        $scope.uploadImage = function() {

            // console.log($scope.file);
            // return;

            if (UserService.getUserInfo() == null) {
                $scope.showNotifyDialog({
                    title: ":)",
                    message: "Bạn vui lòng điền thông tin cá nhân để tiếp tục",
                    btn_success: 1,
                },function(){
                    window.location.reload();
                });


                return;
            }

            if ($scope.uploading)
                return;

            $scope.uploading = true;
            Upload.upload({
                url: RESOURCES.API_SERVICE_GIA + 'save-image-caption',
                data: {
                    file: dataURItoBlob($scope.croppedDataUrl, $scope.file),
                    otherInfo: {
                        caption: $scope.caption
                    }
                },
                headers: {
                    'X-AMB-SID': UserService.getUserSID()
                },
            }).then(function(resp) {
                $scope.uploading = false;
                var data = resp.data;
                $scope.file = "";
                if (data.error == 1) {
                    data.btn_close = 1;
                    data.title = ":(";
                    $scope.showNotifyDialog(data);
                } else if (data.error == 2) {
                    data.btn_close = 1;
                    data.choose_another = 1;
                    data.title = ":(";
                    $scope.showNotifyDialog(data);
                } else if (data.error == 0) {
                    data.btn_close = 1;
                    data.btn_share = 1;
                    data.title = "Chúc mừng";
                    // data.message = "Chúc mừng bạn đã up hình ảnh thành công. Hãy chia sẻ để nhận được <span style=\"color:#0090ff\">3 lượt bốc lì xì</span>";
                    $scope.showNotifyDialog(data);

                    //, và nhận được <span style=\"color:#0090ff\">3 lượt bốc lì xì</span>
                }

            })

        }

        $scope.share = function(img) {
            var sharePath = UserService.getImgSharePath(img);
            FB.ui({
                    method: 'share',
                    href: sharePath,
                },
                function(response) {
                    if (response && !response.error_message) {
                        $scope.sharedImage(img);
                    } else {
                        $scope.shareAgainDialog(img);
                    }
                }
            );

        }

        $scope.sharedImage = function(img) {
            REQUEST.postData(RESOURCES.API_SERVICE_GIA + 'shared-image', {
                img: img
            }, function(response) {
                var data = response.data;
                if (data.error) {
                    //share error
                    $scope.shareAgainDialog(img);
                } else {
                    var data = {};
                    data.btn_close = 1;
                    data.go_to_lixi = 1;
                    data.title = "Chúc mừng";
                    data.message = "Bạn đã đăng thành công khoảnh khắc tết của mình và nhận được <span style=\"color:#0090ff\">3 lượt bốc lì xì</span>. Bốc ngay để nhận may mắn nhé";
                    $scope.showNotifyDialog(data);
                }
                // console.log(data)
            });
        }


        $scope.shareAgainDialog = function(img) {
            var data = {};
            data.btn_share = 1;
            data.btn_close = 1;
            data.title = ":(";
            data.message = "Chia sẻ không thành công. Hãy thử lại để nhận được <span style=\"color:#0090ff\">3 lượt bốc lì xì</span>";
            data.image = img;
            $scope.showNotifyDialog(data);
        }

        $scope.showNotifyDialog = function(data, callback) {

            createDialog("views/dialogs/dialog_upanh_notify.html", {
                id: "dlg-lixi-notify",
                title: "",
                controller: 'notifyDlgCtrl',
                success: {                    
                    fn: function() {
                        if (callback != undefined) {
                            callback();
                        }
                    }
                }
            }, {
                dialogParam: {
                    title: data.title,
                    message: data.message,
                    btn_close: data.btn_close,
                    btn_success: data.btn_success,
                    choose_another: data.choose_another,
                    go_to_lixi: data.go_to_lixi,
                    btn_share: data.btn_share,
                    share: $scope.share,
                    img: data.image
                }
            });
        }



        $scope.$on('$viewContentLoaded', function() {

        });



    }])
