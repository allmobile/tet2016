'use strict';
angular.module('tet2016')
    .filter('html', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    })

.controller('LibraryCtrl', ['createDialog', '$rootScope', '$scope', 'RESOURCES', 'REQUEST', 'UserService', '$controller', '$routeParams', 'localStorageService', '$location', function(createDialogService, $rootScope, $scope, RESOURCES, REQUEST, UserService, $controller, $routeParams, localStorageService, $location) {
    $rootScope.sound_enable = false;
    var referrer = $routeParams.r;
    var img = $routeParams.img;
    console.log(referrer + '-' + img);
    if (referrer != undefined && referrer != '') {
       UserService.setRef(referrer);
    }
    // hien thi hinh anh referrer
    if (referrer && img) {
        REQUEST.postData(RESOURCES.API_GET_IMAGE_BY_NAME + 'image/' + img, {}, function(response) {
            var status = response.status;
            var data = response.data;
            if (status != 200) {
                createDialogService("views/dialogs/dialog_notify.html", {
                    id: "dlg-notify-library",
                    title: "",
                    controller: 'notifyDlgCtrl',

                }, {
                    dialogParam: {
                        message: (data.error_description) ? data.error_description : 'Hình ảnh không hợp lệ',
                        btn_success: 1,

                    }
                });
                $scope.items = {};
            } else {
                $scope.gotoDetail(data.item);
            }
        });
    }


    $scope.search_params = {
        keyword: '',
        display: 3,
        page: 1
    };

    if ($routeParams.page) $scope.search_params.page = $routeParams.page;

    $scope.filter = function(display_type) {
        $scope.search_params.page = 0;

        if ($scope.search_params.display != display_type) $scope.search_params.display = display_type;
        if (!UserService.islogged() && display_type == 2) {
            $scope.search_params.display = 1;
            display_type = 1;
            createDialogService("views/dialogs/dialog_notify.html", {
                id: "dlg-notify-library",
                title: "",
                controller: 'notifyDlgCtrl',

            }, {
                dialogParam: {
                    message: "Bạn phải đăng nhập mới có thể xem ảnh cá nhân",
                    btn_success: 1,

                }
            });
            return;
        }

        $scope.search();
    }



    

    $scope.search = function() {


        REQUEST.postData(RESOURCES.API_IMAGES_SEARCH, $scope.search_params, function(response) {

            var status = response.status;
            var data = response.data;
            if (status != 200) {
                createDialogService("views/dialogs/dialog_notify.html", {
                    id: "dlg-notify-library",
                    title: "",
                    controller: 'notifyDlgCtrl',

                }, {
                    dialogParam: {
                        message: "Hiện tại chưa có dữ liệu. Vui lòng thử lại sau !.",
                        btn_success: 1,

                    }
                });

                $scope.items = {};
                $scope.special_items = {};
            } else {
                $scope.items = data.selfie_list;
                // tu  trang thu 10 khong hien thi may cai stick image nua
                if ($scope.search_params.page <= 10) $scope.special_items = data.list_stick;
                else $scope.special_items = {}
                $scope.paginator = data.panigator;

                $scope.fetchLike();

            }

        });
    };

    $scope.fetchLike = function(){
        if (!UserService.islogged() ) return;
        if($scope.items){
            $scope.items.forEach(function(item) {  
                item.is_like = 0;                              
                if(item.like_user && item.like_user.indexOf(','+ UserService.getUserID() +',') > -1) item.is_like = 1;
                
            });
        }
        if($scope.special_items){
            $scope.special_items.forEach(function(item) {  
                item.is_like = 0;                              
                if(item.like_user && item.like_user.indexOf(','+ UserService.getUserID() +',') > -1) item.is_like = 1;
                
            });
        }

        console.log($scope.items);
        console.log($scope.special_items);
    }



    $scope.search();

    $scope.gotoPage = function(page) {
        //$location.path('/thu-vien/page/'+page);
        $scope.search_params.page = page;
        console.log(page);
        $scope.search();
    }

    $scope.gotoDetail = function(item) {
        createDialogService("views/dialogs/dialog_library.html", {
            id: "dlg-notify-library",
            title: "",
            controller: 'notifyDlgCtrl',
        }, {
            dialogParam: {
                item: item,
                btn_success: 0,
                postLike: $scope.postLike,
                postShare: $scope.postShare
            }
        });
    }

    

    

     

    $scope.postLike = function(item) {
            if (!UserService.islogged()) {

                $rootScope.setLoginCallback( function() {
                        console.log("login success in LibraryCtrl ");
                        $scope.postLike(item);                        
                    }
                );
                $scope.loginDialog();
        
                /*createDialogService("views/dialogs/dialog_notify.html", {
                    id: "dlg-notify-library",
                    title: "",
                    controller: 'notifyDlgCtrl',

                }, {
                    dialogParam: {
                        message: "Bạn phải đăng nhập mới có thể thích hình ảnh",
                        btn_success: 1,

                    }
                });*/
                return;
            }

            REQUEST.postData(RESOURCES.API_POST_LIKE + item.id, {}, function(response) {

                var status = response.status;
                var data = response.data;
                if (status != 200) {
                    createDialogService("views/dialogs/dialog_notify.html", {
                        id: "dlg-notify-library",
                        title: "",
                        controller: 'notifyDlgCtrl',

                    }, {
                        dialogParam: {
                            message: response.data.error_description,
                            btn_success: 1,
                        }
                    });
                } else {
                    item.total_like++;
                    item.is_like = 1;
                    if (data.is_first_like) {
                        createDialogService("views/dialogs/dialog_upanh_notify.html", {
                            id: "dlg-notify-library",
                            title: "",
                            controller: 'notifyDlgCtrl',

                        }, {
                            dialogParam: {
                                title: "Chúc mừng",
                                message: "Bạn đã nhận được 01 lượt bốc lì xì may mắn. Hãy thử ngay vận may của mình nhé!.",
                                btn_close: 1,
                                go_to_lixi: 1,

                            }
                        });
                    }
                }

            });
        } // end post like

    $scope.postShare = function(item) {
        console.log("post share");
        var img = item.image;
        var sharePath = UserService.getImgSharePath(img,item.user_id);

        console.log(sharePath);
        FB.ui({
                method: 'share',
                href: sharePath,
            },
            function(response) {
                if (response && !response.error_message) {
                    console.log("thanh cong share");
                    //item.total_fb_share++;
                } else {
                    console.log("that bai share");
                    //$scope.postShare(item);
                }
            }
        );
    }


}])
