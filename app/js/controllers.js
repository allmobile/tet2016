'use strict';

/**
 * Opens window screen centered.
 * @param windowWidth the window width in pixels (integer)
 * @param windowHeight the window height in pixels (integer)
 * @param windowOuterHeight the window outer height in pixels (integer)
 * @param url the url to open
 * @param wname the name of the window
 * @param features the features except width and height (status, toolbar, location, menubar, directories, resizable, scrollbars)
 */


var contentHeight = 0;
var mainContentHeight = 0;
var headerHeight = 0;
var verticalboxHeight = 0;
var wHeight = $(window).height();
var wWith = $(window).width();

angular.module('tet2016')

.controller('GlobalCtrl', ['$rootScope', 'UserService', '$routeParams', '$location', 'createDialog', '$scope', 'localStorageService', 'REQUEST', 'RESOURCES', function($rootScope, UserService, $routeParams, $location, createDialog, $scope, localStorageService, REQUEST, RESOURCES) {

        var globalCtrl = this;
        var globalCtrl = this;
        var meta = localStorageService.get('meta');
        console.log(meta)
        if (!meta || !meta.cities || !meta.cities.count) {
            REQUEST.getData(RESOURCES.API_META_PATH, function(response) {

                var data = response.data;

                if (data.cities) {
                    localStorageService.set('meta', data);
                }
            });
        }

        $rootScope.no_install = false;
        $rootScope.sound_enable = false;

        $(".header").css('display', 'block');

        $scope.tokenChange = "";

        $scope.gotoHome = function() {
            $location.path("/");
        }


        $scope.gotoUpanh = function() {
            $location.path("/chia-se-khoanh-khac");
        }



        $scope.$on('$routeChangeSuccess', function(next, current) {
            var custom_token = $routeParams.token;
            var is_mobile = $routeParams.mobile;
            var is_story = $routeParams.story;

            if ((is_mobile != "" && is_mobile != undefined) || (is_story != "" && is_story != undefined)) {
                $(".header").css('display', 'none');
                $rootScope.no_install = true;
            }
            if (custom_token != "" && custom_token != undefined) {
                console.log("custom_token:" + custom_token);
                $scope.tokenChange = custom_token;

                $(".header").css('display', 'none');
                $rootScope.no_install = true;
            }


        });


        $scope.gotoSite = function() {

            window.open("http://mobiistar.vn")
        }


        $scope.$on('$viewContentLoaded', function() {


        });

        //
        //
        //




        // createDialog("views/dialogs/dialog_enter_info.html", {
        //     id: "dlg-information",
        //     title: "",
        //     controller: 'InformationDlgCtrl',
        // }, {
        //     dialogParam: {

        //     }
        // });



        $rootScope.setLoginCallback = function(callback) {
            $rootScope.loginCallback = function(userInfo) {
                if (!parseInt(userInfo.is_registered)) {
                    createDialog("views/dialogs/dialog_enter_info.html", {
                        id: "dlg-information",
                        title: "",
                        controller: 'InformationDlgCtrl',
                    }, {
                        dialogParam: {
                            userInfo: userInfo,
                            onRegisterSuccess: function(userInfo) {
                                UserService.setUserInfo(userInfo);
                                if (callback != undefined)
                                    callback();
                            }
                        }
                    });
                } else {
                    UserService.setUserInfo(userInfo);
                    if (callback != undefined)
                        callback();
                }
            }

            UserService.loginCallback = {
                success: function(userInfo) {
                    console.log("login success in ");
                    console.log(userInfo);
                    $rootScope.loginCallback(userInfo);
                },
                fail: function() {
                    console.log("login fail");
                }
            };

        }



        $rootScope.setLoginCallback(function() {
            // window.location.reload();
        });

        $scope.loginDialog = function() {

            jQuery('.navbar-toggle[aria-expanded="true"]:visible').click();

            if (!UserService.getUserInfo()) {
                // chua login-hien dialog
                createDialog("views/dialogs/dialog_login.html", {
                    id: "dlg-login",
                    title: "",
                    controller: 'notifyDlgCtrl',
                }, {
                    dialogParam: {}
                });

            } else {
                console.log("user registered");
                $rootScope.loginCallback(UserService.getUserInfo());
            }

        }



        $scope.$watch('tokenChange', function() {
            console.log("tokenChange:" + $scope.tokenChange);
            if ($scope.tokenChange) {
                UserService.setToken($scope.tokenChange);
                UserService.loadUserInfo(function(userInfo) {
                    if (UserService.loginCallback) {
                        UserService.loginCallback.success(userInfo);

                    }
                }, function() {
                    if (UserService.loginCallback) {
                        UserService.loginCallback.fail();
                    }
                });
            }

        });



        if (UserService.islogged()) {
            console.log(UserService.getToken())
            $scope.tokenChange = UserService.getToken();
        }


        $scope.$on("$routeChangeSuccess", function() {

            setTimeout(function() {
                console.log("sound_enable:" + $rootScope.sound_enable);
                if ($rootScope.sound_enable) {
                    $('#soundBG audio').attr('src', 'sounds/trove.mp3');
                    $('#soundBG').removeClass('off');
                } else {
                    $('#soundBG audio').attr('src', '');
                    $('#soundBG').addClass('off');
                }
            }, 1000)

        });


        $scope.changeSound = function() {
            if ($('#soundBG audio')[0].paused) {
                $scope.playSoundBG();
            } else {
                $scope.pauseSoundBG();
            }
        }

        $scope.pauseSoundBG = function() {
            $('#soundBG').addClass('off');
            $('#soundBG audio')[0].pause();
        }

        $scope.playSoundBG = function() {
            $('#soundBG').removeClass('off');
            $('#soundBG audio')[0].play();
        }


        $scope.showAlertUpdate = function() {
            createDialog("views/dialogs/dialog_notify.html", {
                id: "dlg-notify-library",
                title: "",
                controller: 'notifyDlgCtrl',
                cancel:{
                    fn:function(){
                        localStorageService.set('showed_alert_update', 1);
                    }
                }

            }, {
                dialogParam: {
                    title:"",
                    message: "<p style='font-weight:bold;font-size:20px'>THÔNG BÁO</p><p style='font-weight:bold;font-size:17px'>Hệ thống vừa được cập nhật để tổng hợp điểm số từ lượt like Facebook. Bạn nhớ ghé thăm lại hình ảnh của mình nhé!</p><p  style='font-style:italic;font-size:14px'>*Sau 23h59 - 21/02/2016, hệ thống sẽ đóng băng kết quả để trao thưởng, mọi thay đổi về lượt yêu thích sau thời gian này sẽ không được cập nhật.</p><p style='margin-top:10px'></p>",
                    btn_close: 1,

                }
            });

        }

        if(!localStorageService.get('showed_alert_update')){
            $scope.showAlertUpdate();
        }

        



    }])
    .controller('MainCtrl', ['createDialog', '$rootScope', '$routeParams', '$scope', 'RESOURCES', 'REQUEST', '$controller', 'UserService', 'localStorageService', '$location', function(createDialog, $rootScope, $routeParams, $scope, RESOURCES, REQUEST, $controller, UserService, localStorageService, $location) {

        $scope.is_story = $routeParams.story;
        $rootScope.sound_enable = true;


        $scope.slidesPc = [{
            'img': 'images/slide_home1.png',
            'href': ''
        }, {
            'img': 'images/slide_home2.png',
            'href': ''
        }, {
            'img': 'images/slide_home3.png',
            'href': ''
        }];

        $scope.slidesMobile = [{
            'img': 'images/mobile_slide_home1.png',
            'href': ''
        }, {
            'img': 'images/mobile_slide_home2.png',
            'href': ''
        }, {
            'img': 'images/mobile_slide_home3.png',
            'href': ''
        }];

        $scope.$on('$viewContentLoaded', function() {
            if ($(window).width() < 640)
                $scope.slides = $scope.slidesMobile;
            else
                $scope.slides = $scope.slidesPc;

        });


        var currentDate = new Date();
        console.log((currentDate.getMonth() + 1))
        console.log(currentDate.getDate())
        console.log(currentDate.getFullYear())

        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        if (year == 2016 && month == 2 && (day == 5 || day == 6 || day == 8 || day == 9 || day == 10)) {
            if (localStorageService.get('showed_banner') != day)
                localStorageService.remove('showed_banner');
        } else {
            localStorageService.set('showed_banner', day);
        }

        if (!localStorageService.get('showed_banner') && !$scope.is_story && !$scope.is_mobile && !$scope.custom_token) {
            createDialog("views/dialogs/dialog_tet.html", {
                id: "dlg-lixi-notify",
                title: "",
                controller: 'notifyDlgCtrl',
                cancel: {
                    fn: function() {
                        localStorageService.set('showed_banner', day);
                        $location.path('/li-xi')
                    }
                }
            }, {
                dialogParam: {

                }
            });
        }



        // var count = 0 ;
        // createDialog("views/dialogs/dialog_upanh_notify.html", {
        //     id: "dlg-lixi-notify",
        //     title: "",
        //     controller: 'notifyDlgCtrl',
        // }, {
        //     dialogParam: {
        //         title: "Chúc mừng",
        //         message: count + " người bạn đã tham gia chia sẻ khoảnh khắc trở về cùng bạn, bạn nhận được " + count + " lượt bốc lì xì may mắn.  Bốc ngay và chia sẻ thêm nhiều may mắn nữa nhé!",
        //         btn_close: 1,
        //         go_to_lixi: 1,
        //     }
        // });
        // 
        $scope.playvideo = function() {
            $("#bgvid")[0].pause();
            createDialog("views/dialogs/dialog_video.html", {
                id: "dlg-video",
                title: "",
                backdropCancel: true,
                controller: 'notifyDlgCtrl',
                cancel: {
                    fn: function() {
                        $("#bgvid")[0].play();
                    }
                }

            }, {
                dialogParam: {

                }
            });
        }


        if (UserService.islogged()) {
            REQUEST.getData(RESOURCES.API_GET_SPECIAL_VOUCHER, function(response) {
                var data = response.data;
                if (data.count) {
                    var count = data.count;
                    createDialog("views/dialogs/dialog_upanh_notify.html", {
                        id: "dlg-lixi-notify",
                        title: "",
                        controller: 'notifyDlgCtrl',
                    }, {
                        dialogParam: {
                            title: "Chúc mừng",
                            message: count + " người bạn đã tham gia chia sẻ khoảnh khắc trở về cùng bạn, bạn nhận được " + count + " lượt bốc lì xì may mắn.  Bốc ngay và chia sẻ thêm nhiều may mắn nữa nhé!",
                            btn_close: 1,
                            go_to_lixi: 1,
                        }
                    });
                }
            });
        }



    }])

.controller('LoginCtrl', ['$location', '$rootScope', '$routeParams', 'UserService', '$scope', 'RESOURCES', 'REQUEST', function($location, $rootScope, $routeParams, UserService, $scope, RESOURCES, REQUEST) {
        var slug = $routeParams.slug;
        console.log("slug:" + slug);
        $rootScope.sound_enable = false;
        if (!UserService.islogged()) {
            var param = $location.search();
            if (param.token || param.error_code) {
                if (param.token) {
                    console.log(param);
                    var s = window.opener.angular.element('#ctrl').scope();
                    console.log(s);

                    s.tokenChange = param.token;
                    // UserService.setToken(param.token);
                    s.$apply();
                } else {
                    dialogs.error("Lỗi", param.error_description);
                }
                window.close();
            }


        } else {
            var s = window.opener.angular.element('#ctrl').scope();
            s.tokenChange = UserService.getToken();
            s.$apply();
            window.close();
        }

        // $scope.$on('$viewContentLoaded', function() {
        //     $(".footer .navbar").removeClass("navbar-fixed-bottom");
        // });

    }])
    .controller('RuleCtrl', ['$scope', '$rootScope', '$routeParams', 'RESOURCES', 'REQUEST', 'UserService', function($scope, $rootScope, $routeParams, RESOURCES, REQUEST, UserService) {
        $rootScope.sound_enable = false;

        $scope.mobile = $routeParams.mobile;
        $scope.$on('$viewContentLoaded', function() {
            $(".footer .navbar").removeClass("navbar-fixed-bottom");
        });

    }])
    .controller('PresentsCtrl', ['$scope', '$rootScope', 'RESOURCES', 'REQUEST', 'UserService', function($scope, $rootScope, RESOURCES, REQUEST, UserService) {
        $rootScope.sound_enable = false;
        $(".header").css('display', 'none');

        $scope.$on('$viewContentLoaded', function() {
            $(".footer .navbar").removeClass("navbar-fixed-bottom");
        });

    }])
    .controller('LuckyCtrl', ['$scope', '$rootScope', 'RESOURCES', 'REQUEST', 'dialogs', 'createDialog', function($scope, $rootScope, RESOURCES, REQUEST, dialogs, createDialogService) {
        $rootScope.sound_enable = false;
        $scope.search_info = {};
        $scope.search_url = "";
        $scope.search_param = {};

        $scope.list_programs = [{
                "id": 0,
                "name": "PRIME X Grand"
            },
            // {
            //     "id": 1,
            //     "name": "Prime X Max"
            // }, 
            {
                "id": 2,
                "name": "Game"
            }
        ];
        // theo doi selected_program thay doi
        $scope.$watch('selected_program', function(newVal, oldVal) {
            // if ($scope.search_info.email == undefined)
            //     $scope.search_info.email = "";
            $scope.search_info.email = "";
            switch (newVal) {

                case 0:
                    $scope.search_url = RESOURCES.API_AMB_LUCKY_LIST;
                    $scope.param = {}
                    $scope.param = {
                        event_id: RESOURCES.API_AMB_EVENT_GRAND_ID
                    };
                    break;
                case 1:
                    $scope.search_url = RESOURCES.API_AMB_LUCKY_LIST;
                    $scope.param = {}
                    $scope.param = {
                        event_id: RESOURCES.API_AMB_EVENT_MAX_ID
                    };
                    break;
                case 2:
                    $scope.search_url = RESOURCES.API_LUCKY_LIST;
                    $scope.param = {};
                    $scope.param = {
                        search: $scope.search_info.email
                    };
                    break;
            }

            $scope.search();

        });


        $scope.$watch('search_info.email', function(newVal, oldVal) {
            if ($scope.search_info.email == undefined)
                $scope.search_info.email = "";
            $scope.param.email = $scope.search_info.email;
            $scope.param.search = $scope.search_info.email;
            $scope.search_final_url = $scope.search_url + "?" + REQUEST.buildQueryString($scope.param);
        });


        $scope.search = function() {
            console.log($scope.search_info);

            $scope.search_final_url = $scope.search_url + "?" + REQUEST.buildQueryString($scope.param);

            REQUEST.postData($scope.search_final_url, $scope.search_param, function(response) {

                var status = response.status;
                var data = response.data;
                console.log(status);
                if (status != 200) {
                    createDialogService("views/dialog_notify.html", {
                        id: "dlg-notify",
                        title: "",
                        controller: 'notifyDlgCtrl',

                    }, {
                        dialogParam: {
                            message: "Hiện tại chưa có dữ liệu. Vui lòng thử lại sau !.",
                            btn_success: 1,

                        }
                    });

                    $scope.data = {};

                } else {
                    var err_code = data.error_code;
                    var err_desc = data.error_description;
                    if (err_code > 0) {
                        createDialogService("views/dialog_notify.html", {
                            id: "dlg-notify",
                            title: "",
                            controller: 'notifyDlgCtrl',

                        }, {
                            dialogParam: {
                                message: err_desc,
                                btn_success: 1,

                            }
                        });

                    } else {
                        if (data.length) {
                            $scope.data = data;
                        } else {
                            if ($scope.search_info.email) {
                                //searching
                                createDialogService("views/dialog_notify.html", {
                                    id: "dlg-notify",
                                    title: "",
                                    controller: 'notifyDlgCtrl',
                                    success: {
                                        label: 'Đồng ý',
                                        fn: function() {
                                            $scope.search_info.email = "";
                                            $scope.search(1);
                                        }
                                    }

                                }, {
                                    dialogParam: {
                                        message: "Dữ liệu bạn tìm không tồn tại !",
                                        btn_success: 1,
                                    }
                                });
                            } else {
                                createDialogService("views/dialog_notify.html", {
                                    id: "dlg-notify",
                                    title: "",
                                    controller: 'notifyDlgCtrl',


                                }, {
                                    dialogParam: {
                                        message: "Hiện tại chưa có dữ liệu. Vui lòng thử lại sau !.",
                                        btn_success: 1,


                                    }
                                });
                            }
                        }
                    }
                }

            });




            return false;
        }



        $scope.$on('$viewContentLoaded', function() {
            $(".footer .navbar").removeClass("navbar-fixed-bottom");
        });

    }])
    .controller('ThuVienCtrl', ['$scope', '$rootScope', 'RESOURCES', 'REQUEST', function($scope, $rootScope, RESOURCES, REQUEST) {
        $rootScope.sound_enable = false;
        $scope.search_params = {
            keyword: '',
            display: 1
        };
        console.log('thu vien ');

        $scope.normal_images = [1, 2, 3, 4, 5, 6, 7, 8];

        $scope.changeDisplay = function(display_type) {
            $scope.search_params.display = display_type;
        }


    }])
    .controller('AboutCtrl', function() {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
    });
