'use strict';


var sCurrentLeft, sCurrentRight, job;

function swing(num) {


    sCurrentLeft = "imgLeft" + num;
    sCurrentRight = "imgRight" + num;
    if (!$('.swing' + num).hasClass("animation")) {

        var leftOrright = Math.floor(Math.random() * 2) + 1;
        $('.swing' + num).addClass("animation");
        $('.swing' + num).addClass(sCurrentRight);
    } else if ($('.swing' + num).hasClass(sCurrentRight)) {
        $('.swing' + num).removeClass(sCurrentRight);
        $('.swing' + num).addClass(sCurrentLeft);
    } else {
        $('.swing' + num).removeClass(sCurrentLeft);
        $('.swing' + num).addClass(sCurrentRight);
    }

    var time = Math.floor(Math.random() * 2) + 1
    job = setTimeout(function() {
        swing(num)
    }, 1000);
}

function swing(object, num, job, timedelay) {
    //console.log("swing:" + num)
    sCurrentLeft = "imgLeft5";
    sCurrentRight = "imgRight5";
    if (!object.hasClass("animation")) {
        var leftOrright = Math.floor(Math.random() * 2) + 1;
        object.addClass("animation");
        object.addClass(sCurrentRight);
    } else if (object.hasClass(sCurrentRight)) {
        object.removeClass("imgcenter");
        object.removeClass(sCurrentRight);
        object.addClass(sCurrentLeft);
    } else if (object.hasClass("imgcenter")) {
        object.removeClass("imgcenter");
        object.addClass(sCurrentLeft);
    } else {
        //object.removeClass("imgcenter");
        object.removeClass(sCurrentLeft);
        object.addClass(sCurrentRight);
    }

    if (num == 5) {
        //console.log("num:" + num)
        object.removeClass("animation");
        object.addClass("imgcenter");
        object.removeClass(sCurrentLeft);
        object.removeClass(sCurrentRight);
        clearTimeout(job);
        num = 0;
    } else {
        var time = Math.floor(Math.random() * 2) + 1
        job = setTimeout(function() {
            swing(object, num + 1, job, 1000)
        }, 1000);
    }


}


angular.module('tet2016')
    .controller('LixiCtrl', ['createDialog', '$rootScope','$routeParams', '$scope', 'RESOURCES', 'REQUEST', '$controller', 'UserService', function(createDialog, $rootScope, $routeParams,$scope, RESOURCES, REQUEST, $controller, UserService) {

        $scope.is_dsmm = $routeParams.dsmm;
        $rootScope.sound_enable = false;
     
        $scope.new_year_posts = ['Đầu năm vui vẻ, suôn sẻ cả năm! Luôn mỉm cười để Tết thêm tươi, rạng ngời may mắn cùng Mobiistar nhé!',
        'May mắn có xa, kiên trì mới nhận nhiều quà! Vui Tết thả ga, yêu thương lan tỏa cùng Mobiistar.',
        'Vui Tết cùng Mobiistar, may mắn lan tỏa, yêu thương vang xa.'];

        $scope.baolixiListMobile = [{
            'class': 'bao12'
        }, {
            'class': 'bao3',
            'style': {

            }
        }, {
            'class': 'bao6',
            'style': {

            }
        }, {
            'class': 'bao7',
            'style': {

            }
        }, {
            'class': 'bao9',
            'style': {

            }
        }, {
            'class': 'bao15',
            'style': {

            }
        }, {
            'class': 'bao19',
            'style': {

            }
        }, {
            'class': 'bao26',
            'style': {

            }
        }];


        $scope.baolixiListPC = [{
            'class': 'bao12',
            'style': {

            }
        }, {
            'class': 'bao3',
            'style': {

            }
        }, {
            'class': 'bao6',
            'style': {

            }
        }, {
            'class': 'bao7',
            'style': {

            }
        }, {
            'class': 'bao9',
            'style': {

            }
        }, {
            'class': 'bao15',
            'style': {

            }
        }, {
            'class': 'bao19',
            'style': {

            }
        }, {
            'class': 'bao26',
            'style': {

            }
        }, {
            'class': 'bao29',
            'style': {

            }
        }, {
            'class': 'bao32',
            'style': {

            }
        }, {
            'class': 'bao34',
            'style': {

            }
        }, {
            'class': 'bao39',
            'style': {

            }
        }, {
            'class': 'bao44',
            'style': {

            }
        }, {
            'class': 'bao47',
            'style': {

            }
        }, {
            'class': 'bao53',
            'style': {

            }
        }, {
            'class': 'bao91',
            'style': {

            }
        }, {
            'class': 'bao31',
            'style': {

            }
        }, {
            'class': 'bao341',
            'style': {

            }
        }]


        if ($(window).width() < 640)
            $scope.baolixiList = $scope.baolixiListMobile;
        else
            $scope.baolixiList = $scope.baolixiListPC;



        $scope.$on('$viewContentLoaded', function() {
            setInterval(function() {
                $('.tree-mai div').removeClass("myswing");
                var total = $scope.baolixiList.length;
                var num = Math.floor(Math.random() * total) + 1;
                $('.tree-mai div').each(function(index) {
                    if (!$(this).hasClass("animation") && index < num) {
                        swing($(this), 1, null, 1000);
                    }
                });
            }, 3000);



            setTimeout(function() {
                $('.tree-mai div').unbind("hover");
                $('.tree-mai div').hover(function() {
                    console.log("hover");
                    // $(this).css("transform","scale(1.5)")
                    $(this).removeClass("animation imgcenter imgLeft imgRight imgLeft5 imgRight5");
                    $(this).addClass("tada animated infinity")
                        // swing($(this), 1, null, 100);
                }, function() {
                    // $(this).css("transform","scale(1)")
                    $(this).removeClass("tada animated infinity")
                });
            }, 100)

        });



        $rootScope.setLoginCallback(function() {
            $scope.initLucky();
        });


        $scope.$watch('numplay', function() {
            jQuery("#num").text($scope.numplay);
        });




        $scope.notifyDialog = function(title, message, callback) {
            createDialog("views/dialogs/dialog_upanh_notify.html", {
                id: "dlg-lixi-notify",
                title: "",
                controller: 'notifyDlgCtrl',
                success: {
                    label: 'Đồng ý',
                    fn: function() {
                        if (callback != undefined) {
                            callback();
                        }
                    }
                }
            }, {
                dialogParam: {
                    title: title,
                    message: message,
                    btn_close: 1,
                }
            });

            // createDialog("views/dialogs/dialog_notify.html", {
            //     id: "dlg-lixi-notify",
            //     title: "",
            //     controller: 'notifyDlgCtrl',
            //     success: {
            //         label: 'Đồng ý',
            //         fn: function() {
            //             if (callback != undefined) {
            //                 callback();
            //             }
            //         }
            //     }
            // }, {
            //     dialogParam: {
            //         message: message,
            //         btn_close: 1
            //     }
            // });


        }



        $scope.numplay = 0;
        $scope.initLucky = function() {
            REQUEST.getData(RESOURCES.API_LUCKY_COUNT, function(responseCounter) {
                console.log(responseCounter);
                var data = responseCounter.data;


                $scope.numplay = data.counter;
                jQuery(".tree-mai div.baolixi").unbind("click");
                jQuery(".tree-mai div.baolixi").click(function() {
                    if (!data.counter) {

                        $scope.showDialogWithNewYearPost(":(", "Bạn đã hết lượt bốc lì xì", function() {
                            window.isRolling = false;
                            window.isAllPlay = true;

                        });

                        return;

                    }
                    // jQuery(this).hide();
                    if (window.isRolling == true) {
                        return;
                    }

                    if (window.isAllPlay) {
                        $scope.showDialogAllPlay();
                        return;
                    }

                    REQUEST.getData(RESOURCES.API_LUCKY_GET, function(responseLucky) {
                        var response = responseLucky.data;
                        if (responseLucky.status != 200) {
                            $scope.notifyDialog("", "Có lỗi xảy ra", function() {
                                window.isRolling = false;
                                return;
                            });

                        } else {
                            if (response == "") {
                                window.isRolling = false;
                                $scope.notifyDialog("", "Có lỗi xảy ra", function() {
                                    window.isRolling = false;
                                    return;
                                });

                                return;
                            }

                            var data = responseLucky.data;

                            if (data != null) {

                                if (data.count != null)
                                    $scope.numplay = data.count;
                                else
                                    $scope.numplay = $scope.numplay - 1;

                                setTimeout(function() {
                                    $scope.$apply();
                                }, 0);


                                if (data.count == 0) {
                                    window.isAllPlay = true;
                                }

                                if (data.status == 0) {
                                    $scope.showDialogWithNewYearPost(":(", "Bạn chưa may mắn trong bao lì xì này", function() {
                                        window.isRolling = false;
                                        return;
                                    });


                                } else if (data.status == 2) {
                                    window.isRolling = false;
                                    $scope.showDialogWithNewYearPost(":(", "Bạn đã hết lượt bốc lì xì", function() {
                                        window.isRolling = false;
                                        window.isAllPlay = true;

                                    });


                                } else {
                                    if (data.type == 1) {
                                        //off
                                        $scope.showDialogVoucher(data);

                                    } else if (data.type == 2) {
                                        // card
                                        $scope.showDialogChooseCard(data);

                                    } else if (data.type == 0) {
                                        $scope.showDialogCongratilations(data);
                                    } else {
                                        $scope.showDialogWithNewYearPost(":)", "Bạn chưa may mắn trong bao lì xì này", function() {
                                            window.isRolling = false;
                                            return;
                                        });
                                    }

                                }
                            } else {
                                $scope.showDialogWithNewYearPost(":)", "Bạn chưa may mắn trong bao lì xì này", function() {
                                    window.isRolling = false;
                                    return;
                                });
                            }
                        }


                    });




                });


            });

        }


        if(!$scope.is_dsmm || $scope.is_dsmm == undefined){

           setTimeout(function() {
            $scope.initLucky();
            if (!UserService.islogged()) {
                jQuery(".tree-mai div.baolixi").click(function() {
                    // console.log("fsfsdfsffds");
                    $scope.loginDialog()
                });
            }
        }, 500); 
        }
        


            $scope.share = function(gift_name) {
                FB.ui({
                    method: 'feed',
                    link: "http://tetladetrove.mobiistar.vn/",
                    caption: 'Tết là để trở về - Share khoảnh khắc, bắt lộc xuân',
                    picture: 'http://tetladetrove.mobiistar.vn/share.png?v=1',
                    description: "Năm mới của mình sẽ rất may mắn vì mình đã nhận được " + gift_name +" từ Cây Lì Xì của Mobiistar! Tham gia cùng mình để nhận ngay may mắn trong mỗi bao lì xì nhé!"
                }, function(response) {

                });
            }




        $scope.showDialogCongratilations = function(data) {
            createDialog("views/dialogs/dialog_congratulations.html", {
                id: "dlg-lixi-notify",
                title: "",
                controller: 'notifyDlgCtrl',
            }, {
                dialogParam: {
                    share:$scope.share,
                    gitf: {
                        name: data.name,
                        id: data.gift_id,
                    }

                }
            });
        }

        // $scope.showDialogCongratilations({name:"fdsfdf",gift_id:2});

        $scope.showDialogVoucher = function(data) {
            createDialog("views/dialogs/dialog_voucher.html", {
                id: "dlg-lixi-notify",
                title: "",
                controller: 'notifyDlgCtrl',
            }, {
                dialogParam: {
                    btn_close: 1,
                    share:$scope.share,
                    voucher: {
                        name: data.name,
                        text: data.code
                    }

                }
            });
        }

        // $scope.showDialogVoucher({name:"fdsfdf",gift_id:3})

        $scope.showDialogAllPlay = function() {            
            $scope.showDialogWithNewYearPost(":(", "Bạn đã hết lượt bốc lì xì hôm nay.")
        }

       

        $scope.showDialogWithNewYearPost = function(title,message,callback){
            var post = $scope.new_year_posts[Math.floor(Math.random()*$scope.new_year_posts.length)];
            $scope.notifyDialog(title, "<p>"+message+"</p><p style=\"color:#0090ff\">" + post + "</p>",callback);
        }


         // $scope.showDialogAllPlay();


        $scope.showDialogChooseCard = function(data) {
            createDialog("views/dialogs/dialog_phone_card.html", {
                id: "dlg-phone-card",
                title: "",
                backdropCancel: false,
                controller: 'chooseCardCtrl',
                success: {
                    label: 'Đồng ý',
                    fn: function() {

                    }
                }
            }, {
                dialogParam: {
                    share:$scope.share,
                    btn_close: 1,
                    name: data.name
                }
            });
        }

        $scope.search_params = {
            owner: 0
        };

        // $scope.showDialogChooseCard({name:"fdsfdf",gift_id:3});
        
        $scope.filter = function(owner) {
            $scope.search_params.owner = owner;
            $scope.search();
        }
        $scope.search = function() {
            $scope.data = {};
            if ($scope.search_params.keyword == undefined)
                $scope.search_params.keyword = "";

            //$scope.search_url = API_LUCKY_LIST
            $scope.search_url = RESOURCES.API_LUCKY_LIST + '?search=' + $scope.search_params.keyword + '&email=' + $scope.search_params.keyword;
            $scope.search_url += '&owner=' + $scope.search_params.owner;
            REQUEST.postData($scope.search_url, {}, function(response) {

                var status = response.status;
                var data = response.data;
                //console.log(status);
                if (status != 200) {
                    createDialog("views/dialogs/dialog_notify.html", {
                        id: "dlg-notify",
                        title: "",
                        controller: 'notifyDlgCtrl',

                    }, {
                        dialogParam: {
                            message: "Hiện tại chưa có dữ liệu. Vui lòng thử lại sau !.",
                            btn_success: 1,

                        }
                    });

                    $scope.data = {};

                } else {
                    var err_code = data.error_code;
                    var err_desc = data.error_description;
                    if (err_code > 0) {
                        createDialog("views/dialogs/dialog_notify.html", {
                            id: "dlg-notify",
                            title: "",
                            controller: 'notifyDlgCtrl',

                        }, {
                            dialogParam: {
                                message: err_desc,
                                btn_success: 1,

                            }
                        });

                    } else {
                        if (data.length) {
                            $scope.data = data;
                        } else {
                            if ($scope.search_params.keyword) {
                                //searching
                                createDialog("views/dialogs/dialog_notify.html", {
                                    id: "dlg-notify",
                                    title: "",
                                    controller: 'notifyDlgCtrl',
                                    success: {
                                        label: 'Đồng ý',
                                        fn: function() {
                                            $scope.search_params.keyword = "";
                                            $scope.search(1);
                                        }
                                    }

                                }, {
                                    dialogParam: {
                                        message: "Dữ liệu bạn tìm không tồn tại !",
                                        btn_success: 1,
                                    }
                                });
                            } 
                            // else {
                            //     createDialog("views/dialogs/dialog_notify.html", {
                            //         id: "dlg-notify",
                            //         title: "",
                            //         controller: 'notifyDlgCtrl',


                            //     }, {
                            //         dialogParam: {
                            //             message: "Hiện tại chưa có dữ liệu. Vui lòng thử lại sau !.",
                            //             btn_close: 1,


                            //         }
                            //     });
                            // }
                        }
                    }
                }

            });




            return false;
        }

        $scope.search();

    }])
