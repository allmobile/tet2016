'use strict';


angular.module('tet2016')
    .controller('notifyDlgCtrl', ['$sce','$location', '$scope', '$rootScope', 'dialogParam', 'RESOURCES', 'REQUEST', 'UserService',
        function($sce,$location, $scope, $rootScope, dialogParam, RESOURCES, REQUEST, UserService) {
            $scope.dialogParam = dialogParam;
            $scope.dialogParam.message = $sce.trustAsHtml($scope.dialogParam.message)

            $scope.closeDialog = function() {
                console.log("close dialog");
                window.location.reload();
            }


            $scope.gotoLixi  = function(){
                $location.path('/li-xi');
            }


            $scope.share = function() {
                FB.ui({
                    method: 'feed',
                    link: "http://giovang.mobiistar.vn/",
                    caption: 'Giờ vàng giá tốt: Săn bộ đôi đỉnh - Rinh liền may mắn',
                    picture: 'http://giovang.mobiistar.vn/share_fb.png',
                    description: "Vừa đăng ký thành công smartphone \"Tuyệt Đỉnh Công Phu\" PRIME X Grand với giá ưu đãi, Tết này may mắn rồi đây!"
                }, function(response) {

                });
            }

            $scope.login = function(type) {
                UserService.login(type, {
                    success: function(userInfo) {
                        console.log("login success global");
                        console.log(userInfo);
                        $rootScope.loginCallback(userInfo);
                    },
                    fail: function() {
                        console.log("login fail");
                    }
                });
            }
        }
    ]).controller('InformationDlgCtrl', ['createDialog', 'localStorageService', '$scope', 'dialogParam', 'RESOURCES', 'REQUEST', 'UserService',
        function(createDialog, localStorageService, $scope, dialogParam, RESOURCES, REQUEST, UserService) {
            $scope.dialogParam = dialogParam;
            $scope.register_info = {};
            $scope.register_post_info = {};
            $scope.user_info = $scope.dialogParam.userInfo;
            $scope.callback =  $scope.dialogParam.onRegisterSuccess;

            $scope.initUserInfo = function() {
                if (!$scope.register_info.full_name) {
                    if ($scope.user_info.full_name) {
                        $scope.register_info.full_name = $scope.user_info.full_name;
                    }

                    if ($scope.user_info.job) {
                        $scope.register_info.job = $scope.user_info.job;
                    }

                    if ($scope.user_info.sex) {
                        $scope.register_info.sex = $scope.user_info.sex;
                    }

                    if ($scope.user_info.amb_email) {
                        $scope.register_info.email = $scope.user_info.amb_email;
                    }


                    if ($scope.user_info.email) {
                        $scope.register_info.email = $scope.user_info.email;
                    }

                    if ($scope.user_info.phone) {
                        $scope.register_info.phone = $scope.user_info.phone;
                    }
                }
            }

            $scope.initMeta = function() {
                if (localStorageService.get('meta')) {
                    var meta = localStorageService.get('meta');
                    // $scope.province = meta.cities.items[0];
                    $scope.cities = meta.cities;
                    console.log('initMeta');
                }
            }




            $scope.initMeta();
            $scope.initUserInfo();

            $scope.reset = function() {
                $scope.register_info = {};
                $scope.register_post_info = {};
                $scope.cities = {};
                setTimeout(function() {
                    $scope.initMeta();
                    $scope.$apply();
                }, 500);

            }

            $scope.registerEvent = function() {

                console.log($scope.register_info);

                if ($scope.register_info.full_name && $scope.register_info.sex && $scope.register_info.birthday_day && $scope.register_info.birthday_month && $scope.register_info.birthday_year && $scope.register_info.phone && $scope.register_info.address && $scope.register_info.job) {

                    // $scope.register_info.more_field = $scope.register_info.imei;
                    // var user_info = UserService.getUserInfo();
                    // if (!$scope.register_info.email && user_info.email) {
                    //     $scope.register_info.email = user_info.email;
                    // }

                    $scope.register_info.birthday = $scope.register_info.birthday_day + "-" + $scope.register_info.birthday_month + "-" + $scope.register_info.birthday_year;

                    $scope.register_post_info = angular.copy($scope.register_info);

                    $scope.register_post_info.event_id = RESOURCES.API_EVENT_ID;
                    $scope.register_post_info.is_web = 1;

                    $scope.register_post_info.province = $scope.register_post_info.province.id;
                    $scope.register_post_info.district = $scope.register_post_info.district.id;
                    $scope.register_post_info.referrer_id  = UserService.getRef();
                    

                    REQUEST.postData(RESOURCES.API_USERINFO_UPDATE, $scope.register_post_info, function(response) {

                        var status = response.status;
                        var data = response.data;
                        if (status != 200) {
                            if (status == 401) {
                                $scope.showNotifyDialog("Bạn vui lòng đăng nhập!");
                            } else {
                                var err_desc = data.error_description;
                                $scope.showNotifyDialog(err_desc);
                            }

                        } else {
                            var err_code = data.error_code;
                            var err_desc = data.error_description;
                            if (err_code > 0) {
                                $scope.showNotifyDialog(err_desc);
                            } else {
                                $scope.$modalCancel();
                                $scope.register_post_info.is_registered = 1;
                                UserService.setUserInfo($scope.register_post_info);
                                $scope.showNotifyDialog("Đăng ký thông tin thành công", function() {
                                    if($scope.callback != undefined)
                                        $scope.callback($scope.register_post_info);
                                });

                            }
                        }

                    });
                }


            }


            $scope.showNotifyDialog = function(message, callback) {
                createDialog("views/dialogs/dialog_notify.html", {
                    id: "dlg-notify",
                    title: "",
                    controller: 'notifyDlgCtrl',
                    success: {
                        label: 'Đồng ý',
                        fn: function() {
                            console.log("success callback")
                            if (callback != undefined) {
                                callback();
                            }
                        }
                    }
                }, {
                    dialogParam: {
                        message: message,
                        btn_success: 1
                    }
                });
            }




            console.log(jQuery("#dlg-notify"));



        }
    ])
    .controller('chooseCardCtrl', ['REQUEST', 'RESOURCES', 'createDialog', '$rootScope', '$scope', '$http', 'dialogParam',
        function(REQUEST, RESOURCES, createDialogService, $rootScope, $scope, $http, dialogParam) {
            $scope.card = {
                id: 0,
                text: ""
            }
            $scope.card.id = 0;
            $scope.active = true;
            $scope.dialogParam = dialogParam;

            $scope.closeDialog = function() {
                console.log("close dialog");
                window.location.reload();
            }

            $scope.share = function() {
                FB.ui({
                    method: 'feed',
                    link: "http://tetladetrove.mobiistar.vn/",
                    caption: 'Tết là để trở về - Share khoảnh khắc, bắt lộc xuân',
                    picture: 'http://tetladetrove.mobiistar.vn/share.png?v=1',
                    description: "Năm mới của mình sẽ rất may mắn vì mình đã nhận được [tên giải thưởng] từ Cây Lì Xì của Mobiistar!  Tham gia cùng mình để nhận ngay may mắn trong mỗi bao lì xì nhé!"
                }, function(response) {

                });
            }

            $scope.chooseCard = function(value) {




                $scope.active = false;
                console.log(value);
                $scope.card.text = "Đang lấy mã thẻ...";
                REQUEST.getData(RESOURCES.API_LUCKY_GET_CARD + $scope.card.id, function(response) {
                    var data = response.data;
                    if (data.status == 1 && data.code) {
                        $scope.card.text = data.code;
                    } else if (data.status == 2) {
                        createDialogService("views/dialogs/dialog_notify.html", {
                            id: "dlg-notify",
                            title: "",
                            controller: 'notifyDlgCtrl',
                            success: {
                                label: 'Đồng ý',
                                fn: function() {
                                    $scope.active = true;
                                    $scope.card.id = 0;
                                }
                            }
                        }, {
                            dialogParam: {
                                message: "Đã hết loại thẻ cào bạn chọn. Vui lòng chọn loại thẻ khác",
                                btn_success: 1,

                            }
                        });
                    } else {
                        $scope.card.text = "không lấy được mã thẻ";
                    }
                    return response.data;
                });


                $http.get($rootScope.quayso_get_code + $scope.card.id).then(function(response) {
                    console.log(response);
                    var data = response.data;


                });

            }




        }
    ]);
