'use strict';

/**
 * @ngdoc overview
 * @name mobiVipApp
 * @description
 * # mobiVipApp
 *
 * Main module of the application.
 */

var dialogIntGG, dialogIntFB;
var dialogCountGG = 0;
var dialogCountFB = 0;

function CenterWindow(windowWidth, windowHeight, windowOuterHeight, url, wname, features) {
    var ua = navigator.userAgent;
    var centerLeft = parseInt((window.screen.availWidth - windowWidth) / 2);
    var centerTop = parseInt(((window.screen.availHeight - windowHeight) / 2) - windowOuterHeight);
    var misc_features;
    var win;
    if (features) {
        misc_features = ', ' + features;
    } else {
        misc_features = ', status=no, location=no, scrollbars=yes, resizable=yes';
    }
    var windowFeatures = 'width=' + windowWidth + ',height=' + windowHeight + ',left=' + centerLeft + ',top=' + centerTop + misc_features;
    if (ua.match(/iPhone|iPod|Android|Blackberry.*WebKit/i)) {
        //VERY IMPORTANT - You must use '_blank' and NOT name the window if you want it to work with chrome ios on iphone
        //See this bug report from google explaining the issue: https://code.google.com/p/chromium/issues/detail?id=136610

        win = window.open(url, '_blank');

    } else {
        win = window.open(url, wname, windowFeatures);
    }
    win.focus();
    return win;
}


angular.module('htmlfilters', []).
filter('htmlToPlaintext', function() {
    return function(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});


angular
    .module('tet2016', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'LocalStorageModule',
        'htmlfilters',
        'ui.bootstrap',
        'fundoo.services',
        'ngFileUpload',
        'ngImgCrop',
        'angular-flexslider'

    ])
    .constant('RESOURCES', (function() {
        // Define your variable
        // var apiPath = 'http://api.bietdoianhhungbu.mobiistar.vn';
        var accountAPI = 'http://accounts.allmobile.vn';
        var apiMainPath = 'http://tetladetrove.mobiistar.vn/api-tet';
        var ambApiPath = 'http://api.mobivip.vn';
        var webrootPath = 'http://tetladetrove.mobiistar.vn';
        //
        // console.log(window.location.host);
        var rs = {
            HTML5_MODE: true,
            WEB_PATH: webrootPath,
            API_ACCOUNT_PATH: accountAPI,
            API_META_PATH: 'http://api.mobivip.vn/default/meta',
            API_USER_INFO: apiMainPath + '/user/index/userinfo/?code=',
            API_USERINFO_UPDATE: apiMainPath + '/user/index/update-user-info/',
            API_USER_POINT_INFO: apiMainPath + '/service/service/get-info/',
            API_LUCKY_LIST: apiMainPath + '/service/service/get-lucky-list/',
            API_LUCKY_COUNT: apiMainPath + '/quayso/index/get-counting-lucky/',
            API_LUCKY_GET: apiMainPath + '/quayso/index/get-lucky',
            API_LUCKY_GET_CARD: apiMainPath + '/quayso/index/get-code?card_type=',
            API_AMB_LUCKY_LIST: ambApiPath + '/product/service/get-mbs-register/',
            API_SERVICE_SERVICE: apiMainPath + '/service/service/',
            API_SERVICE_GIA: apiMainPath + '/service/service-gia/',
            API_IMAGES_SEARCH: apiMainPath +'/service/service2/get-selfie/',
            API_IMAGES_STICK_SEARCH: apiMainPath +'/service/service2/get-stick-image/',
            API_POST_LIKE: apiMainPath +'/user/like/post-like/resource_id/',
            API_GET_IMAGE_BY_NAME: apiMainPath +'/service/service2/get-single-image/',
            API_GET_SPECIAL_VOUCHER:apiMainPath + '/service/service2/count-special-voucher-of-user'
        };

        return rs;
    })())
    .service('REQUEST', ['$http', 'RESOURCES', 'UserService', function($http, RESOURCES, UserService) {
        var service = this;

        service.buildQueryString = function(obj, prefix) {
            var str = [];
            for (var p in obj) {
                var k = prefix ? prefix + "[" + p + "]" : p,
                    v = obj[k];
                str.push(angular.isObject(v) ? qs(v, k) : (k) + "=" + encodeURIComponent(v));
            }
            return str.join("&");
        }

        service.postData = function(path, data, callback) {
            jQuery("#spinner").show();
            var req;
            console.log(data);
            if (UserService.getUserSID()) {
                req = {
                    method: 'POST',
                    url: path,
                    data: data,
                    headers: {
                        'X-AMB-SID': UserService.getUserSID()
                    }

                }
            } else {
                req = {
                    method: 'POST',
                    data: data,
                    url: path
                }
            }

            $http(req).then(function(response) {
                jQuery("#spinner").hide();
                callback(response);
            }, function(response) {
                jQuery("#spinner").hide();
                callback(response);
            });
        }

        service.getData = function(path, callback) {
            jQuery("#spinner").show();
            var req;


            if (UserService.getUserSID()) {
                req = {
                    method: 'GET',
                    url: path,
                    headers: {
                        'X-AMB-SID': UserService.getUserSID()
                    }

                }
            } else {
                req = {
                    method: 'GET',
                    url: path
                }
            }


            $http(req).then(function(response) {
                jQuery("#spinner").hide();
                callback(response);
            }, function(response) {
                jQuery("#spinner").hide();
                callback(response);
            });

        }

        function preExcute() {

        }
    }])
    .run(['$rootScope', '$window', 'RESOURCES', function($rootScope, $window, RESOURCES) {

        $rootScope.buildUrl = function(url) {
            var returnUrl = "";
            var urlType = "";


            if (RESOURCES.HTML5_MODE) {
                returnUrl = '/' + url;
            } else
                returnUrl = '#/' + '/' + url;

            return returnUrl;
        }
    }])
    .factory('$FB', ['$rootScope', function($rootScope) {

        var fbLoaded = false;

        // Our own customisations
        var _fb = {
            loaded: fbLoaded,
            _init: function(params) {
                if (window.FB) {
                    // FIXME: Ugly hack to maintain both window.FB
                    // and our AngularJS-wrapped $FB with our customisations
                    angular.extend(window.FB, _fb);
                    angular.extend(_fb, window.FB);

                    // Set the flag
                    _fb.loaded = true;

                    // Initialise FB SDK
                    window.FB.init(params);

                    if (!$rootScope.$$phase) {
                        $rootScope.$apply();
                    }
                }
            }
        }

        return _fb;
    }])
    .factory('UserService', ['$http', '$rootScope', 'localStorageService', 'RESOURCES', function($http, $rootScope, localStorageService, RESOURCES) {
        var sdo = {

        };
        var token = localStorageService.get('token');
        var user_info = localStorageService.get('user_info');

        sdo.loginCallBack = {
            success: function() {},
            fail: function() {}
        };

        sdo.setLoginCallBack = function(callback) {
            sdo.loginCallBack = callback;

        }

        sdo.getRef = function() {
            var refInfo = localStorageService.get('ref');
            return (refInfo && refInfo.ref) ? refInfo.ref : '';
        }


        sdo.setRef = function(strRef) {
            localStorageService.remove('ref');
            var refInfo = {
                ref: strRef,
                t: Date.now()
            }
            localStorageService.set('ref', refInfo);
        }


        sdo.clearIdentity = function() {
            localStorageService.remove('token');
            localStorageService.remove('user_info');
            localStorageService.remove('sid');
        }

        sdo.getUserSID = function() {
            var token = localStorageService.get('sid');
            if (token != null)
                return token;
            return "";
        }

        sdo.setUserSID = function(sid) {
            localStorageService.set('sid', sid);
        }

        sdo.getUserInfo = function() {
            var user_info = localStorageService.get('user_info');
            return (user_info != null && user_info != "") ? user_info : null;
        }

        sdo.getUserID = function() {
            var user_info = localStorageService.get('user_info');
            return (user_info != null && user_info != "") ? user_info.id : "";
        }


        sdo.getToken = function() {

            var token = localStorageService.get('token');
            console.log("getToken:" + token);
            return token;
        }

        sdo.setToken = function(token) {
            localStorageService.set('token', token);

        }

        sdo.setUserInfo = function(user) {
            localStorageService.set('user_info', user);

        }

        sdo.islogged = function() {
            var token = localStorageService.get('token');
            if (!token)
                return false;
            return true;
        }

        sdo.loadUserInfo = function(successFunc, failFunc) {
            var req = {
                method: 'GET',
                url: RESOURCES.API_USER_INFO + sdo.getToken(),
                headers: {
                    'X-AMB-SID': sdo.getUserSID()
                }
            }

            $http(req).then(function(response) {
                jQuery("#spinner").hide();
                var data = response.data;

                if (data.user) {
                    sdo.setUserSID(data.user.sid);
                    if (data.register_info) {
                        successFunc(data.register_info);
                    } else {
                        successFunc(data.user);
                    }

                } else {
                    sdo.clearIdentity();
                    failFunc();
                }

            }, function(response) {
                jQuery("#spinner").hide();
                failFunc();
            });


        }
        sdo.login = function(type, callback) {
            sdo.loginCallback = callback;
            if (sdo.islogged() && sdo.getUserInfo() != null) {
                var userInfo = sdo.getUserInfo();
                if (sdo.loginCallback) {
                    sdo.loginCallback.success(userInfo);
                }
                return;
            }
            switch (type) {
                case 'google':
                    if (dialogIntGG && dialogCountGG > 2) {
                        dialogIntGG.focus();
                    } else {
                        dialogIntGG = CenterWindow(1000, 800, 50, RESOURCES.API_ACCOUNT_PATH + '/user/oauth/social-login?type=google&key=59595dfc8ca5f0385eae63aff98daef9bef2e974bf2ac496c6eb9ac7b39f09639639910b97e47235e758d0e03585be&redirect_success_uri=' + RESOURCES.WEB_PATH + '/login', 'Login');
                        dialogCountGG++;
                        dialogIntGG.onbeforeunload = function() {
                            dialogIntGG = null
                        }
                    }
                    break;
                case 'facebook':
                    if (dialogIntFB && dialogCountFB > 2) {
                        dialogIntFB.focus();
                    } else {
                        dialogIntFB = CenterWindow(1000, 800, 50, RESOURCES.API_ACCOUNT_PATH + '/user/oauth/social-login?type=facebook&package_name=vn.allmobile.gallery&key=59595dfc8ca5f0385eae63aff98daef9bef2e974bf2ac496c6eb9ac7b39f09639639910b97e47235e758d0e03585be&redirect_success_uri=' + RESOURCES.WEB_PATH + '/login', 'Login');
                        dialogCountFB++;
                        dialogIntFB.onbeforeunload = function() {
                            dialogIntFB = null
                        }
                    }

                    break;
            }



        }

        sdo.getImgSharePath = function(img,user_id) {
            if(!user_id)
                user_id = sdo.getUserID();
            return RESOURCES.WEB_PATH + '/share.php?img=' + img + '&r=' + user_id;
        }

        return sdo;
    }])

.directive('fb', ['$FB', function($FB) {
        return {
            restrict: "E",
            replace: true,
            template: "<div id='fb-root'></div>",
            compile: function(tElem, tAttrs) {
                return {
                    post: function(scope, iElem, iAttrs, controller) {
                        var fbAppId = iAttrs.appId || '';

                        var fb_params = {
                            appId: iAttrs.appId || "",
                            cookie: iAttrs.cookie || true,
                            status: iAttrs.status || true,
                            xfbml: iAttrs.xfbml || true
                        };

                        // Setup the post-load callback
                        window.fbAsyncInit = function() {
                            $FB._init(fb_params);
                            console.log(iAttrs);
                            if ('fbInit' in iAttrs) {

                                scope.$apply(iAttrs.fbInit);
                            }
                        };

                        (function(d, s, id, fbAppId) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.async = true;
                            js.src = "//connect.facebook.net/en_US/all.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk', fbAppId));
                    }
                }
            }
        };
    }])
    .directive('bsActiveLink', ['$location', function($location) {
        return {
            restrict: 'A', //use as attribute 
            replace: false,
            link: function(scope, elem) {


                scope.$on("$routeChangeSuccess", function() {
                    console.log("bsActiveLink:routeChangeSuccess");
                    var hrefs = ['/#' + $location.path(),
                        '#' + $location.path(), //html5: false
                        $location.path()
                    ];


                    angular.forEach(elem.find('a'), function(a) {

                        a = angular.element(a);

                        if (-1 !== hrefs.indexOf(a.attr('href'))) {
                            a.parent().addClass('active');
                        } else {
                            a.parent().removeClass('active');
                        };

                    });

                    if ($location.path() != "/")
                        jQuery('.navbar-toggle[aria-expanded="true"]:visible').click();
                });
            }
        }
    }])
    .directive('googleAnalytics', ['$location', '$window', function($location, $window) {
        return {
            restrict: 'A',
            scope: true,
            link: function(scope) {
                scope.$on('$routeChangeSuccess', function() {
                    console.log("routeChangeSuccess:" + $location.url());

                    $window.ga('send', 'pageview', {
                        page: $location.url()
                    });

                    $window.fbq('track', "PageView");
                });
            }
        };
    }])
    .directive('accountArea', ['$compile', 'UserService', function($compile, UserService) {
        return {
            restrict: 'E',
            replace: true,
            template: '',
            link: function(scope, element, attrs, ngModelCtrl) {
                var template = {
                    'logged': '<a  class="account" my-href="buildUrl(\'tai-khoan\')">{{userInfo.full_name}}</a>',
                    'default': '<a ng-click="loginDialog()" class="login">Đăng nhập</a>'
                };
                var templateObj;
                console.log("UserService");
                console.log(UserService.getUserInfo())

                scope.$watch(

                    // This is the important part
                    function() {
                        return UserService.getUserInfo();
                    },

                    function(newValue, oldValue) {
                        if (UserService.getUserInfo() != null) {
                            scope.userInfo = UserService.getUserInfo();
                            templateObj = $compile(template['logged'])(scope);
                        } else {
                            templateObj = $compile(template['default'])(scope);
                        }

                        element.html(templateObj);
                    },
                    true
                );


            }
        };
    }])
    .directive('classRoute', ['$rootScope', '$route', function($rootScope, $route) {

        return function(scope, elem, attr) {
            var previous = '';
            $rootScope.$on('$routeChangeSuccess', function(event, currentRoute) {

                var route = currentRoute.$$route;
                if (route) {

                    var cls = route['class'];

                    if (previous) {
                        attr.$removeClass(previous);
                    }

                    if (cls) {

                        previous = cls;
                        attr.$addClass(cls);
                    }
                }
            });
        };

    }])
    .directive('myHref', ['$parse', function($parse) {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                var url = $parse(attrs.myHref)(scope);
                element.attr('href', url);
            }
        }
    }])
    .directive('whenReady', ['$interpolate', function($interpolate) {
        return {
            restrict: 'A',
            priority: Number.MIN_SAFE_INTEGER, // execute last, after all other directives if any.
            link: function($scope, $element, $attributes) {
                var expressions = $attributes.whenReady.split(';');
                var waitForInterpolation = false;
                var hasReadyCheckExpression = false;

                function evalExpressions(expressions) {
                    expressions.forEach(function(expression) {
                        $scope.$eval(expression);
                    });
                }

                if ($attributes.whenReady.trim().length === 0) {
                    return;
                }

                if ($attributes.waitForInterpolation && $scope.$eval($attributes.waitForInterpolation)) {
                    waitForInterpolation = true;
                }

                if ($attributes.readyCheck) {
                    hasReadyCheckExpression = true;
                }

                if (waitForInterpolation || hasReadyCheckExpression) {
                    requestAnimationFrame(function checkIfReady() {
                        var isInterpolated = false;
                        var isReadyCheckTrue = false;

                        if (waitForInterpolation && $element.text().indexOf($interpolate.startSymbol()) >= 0) { // if the text still has {{placeholders}}
                            isInterpolated = false;
                        } else {
                            isInterpolated = true;
                        }

                        if (hasReadyCheckExpression && !$scope.$eval($attributes.readyCheck)) { // if the ready check expression returns false
                            isReadyCheckTrue = false;
                        } else {
                            isReadyCheckTrue = true;
                        }

                        if (isInterpolated && isReadyCheckTrue) {
                            evalExpressions(expressions);
                        } else {
                            requestAnimationFrame(checkIfReady);
                        }

                    });
                } else {
                    evalExpressions(expressions);
                }
            }
        };
    }])
    .config(['$routeProvider', 'localStorageServiceProvider', '$locationProvider', 'RESOURCES', function($routeProvider, localStorageServiceProvider, $locationProvider, RESOURCES) {



        localStorageServiceProvider
            .setPrefix('amb_tet2016');

        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                'class': 'main'
            })
            .when('/story/:story', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                'class': 'main'
            })

        .when('/chia-se-khoanh-khac', {
                templateUrl: 'views/upanh.html',
                controller: 'UpanhCtrl',
                'class': 'upanh'

            })
            .when('/li-xi', {
                templateUrl: 'views/lixi.html',
                controller: 'LixiCtrl',
                'class': 'lixi'

            })
            .when('/thu-vien', {
                templateUrl: 'views/thu-vien.html',
                controller: 'LibraryCtrl',
                'class': 'thuvien'

            })
            .when('/thu-vien/page/:page', {
                templateUrl: 'views/thu-vien.html',
                controller: 'LibraryCtrl',
                'class': 'thuvien'
            })
            .when('/thu-vien/:r/:img', {
                templateUrl: 'views/thu-vien.html',
                controller: 'LibraryCtrl',
                'class': 'thuvien'

            })
            .when('/li-xi/:token', {
                templateUrl: 'views/lixi.html',
                controller: 'LixiCtrl',
                'class': 'lixi'

            })
            .when('/li-xi/:token/:dsmm', {
                templateUrl: 'views/lixi.html',
                controller: 'LixiCtrl',
                'class': 'lixi'

            })
            .when('/the-le', {
                templateUrl: 'views/rule.html',
                controller: 'RuleCtrl',
                'class': 'rule'

            })
            .when('/the-le/:mobile', {
                templateUrl: 'views/rule.html',
                controller: 'RuleCtrl',
                'class': 'rule'

            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })

        .when('/login', {
                templateUrl: 'views/main-login.html',
                controller: 'LoginCtrl'
            })
            .when('/giai-thuong', {
                templateUrl: 'views/present-mobile.html',
                controller: 'PresentsCtrl',
                'class': 'giaithuong'
            })
            .when('/login/:slug', {
                templateUrl: 'views/main-login.html',
                controller: 'LoginCtrl'
            })
            // .when('/danh-sach-may-man', {
            //     templateUrl: 'views/lucky.html',
            //     controller: 'LuckyCtrl',
            //     'class': 'lucky'
            // })
            // .when('/game', {
            //     templateUrl: 'views/main-quiz.html',
            //     controller: 'QuizCtrl',

        // })

        .otherwise({
            redirectTo: '/'
        });

        // use the HTML5 History API
        if (RESOURCES.HTML5_MODE)
            $locationProvider.html5Mode(true);
    }]);
